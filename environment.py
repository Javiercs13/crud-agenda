import os
from dotenv import load_dotenv
from distutils.util import strtobool


class Environment():

    def __init__(self):
        load_dotenv()

    def general(self):
        return{
            'PORT': int(os.getenv("PORTAPI", 8081)),
            'DEBUG': strtobool(os.getenv('DEBUG', False))
        }

    def db(self):
        return{
            'HOST': os.getenv("HOSTDB", "localhost"),
            'PORT': int(os.getenv("PORTDB", 27017)),
        }
